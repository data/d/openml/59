# OpenML dataset: ionosphere

https://www.openml.org/d/59

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Space Physics Group, Applied Physics Laboratory, Johns Hopkins University. Donated by Vince Sigillito.  
**Source**: [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/ionosphere)  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html) 

**Johns Hopkins University Ionosphere database**  
This radar data was collected by a system in Goose Bay, Labrador.  This system consists of a phased array of 16 high-frequency antennas with a total transmitted power on the order of 6.4 kilowatts.  See the paper for more details.  

### Attribute information
Received signals were processed using an autocorrelation function whose arguments are the time of a pulse and the pulse number.  There were 17 pulse numbers for the Goose Bay system.  Instances in this database are described by 2 attributes per pulse number, corresponding to the complex values returned by the function resulting from the complex electromagnetic signal.

The targets were free electrons in the ionosphere.  "Good" (g) radar returns are those showing evidence of some type of structure in the ionosphere.  "Bad" (b) returns are those that do not; their signals pass through the ionosphere.  

### Relevant papers  
Sigillito, V. G., Wing, S. P., Hutton, L. V., & Baker, K. B. (1989). Classification of radar returns from the ionosphere using neural networks. Johns Hopkins APL Technical Digest, 10, 262-266.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/59) of an [OpenML dataset](https://www.openml.org/d/59). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/59/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/59/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/59/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

